<form action="advance.php" method="GET">
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Gender : </label>
		<div class="col-sm-7 form_radios">
			<input type="radio" name="gender" value="male" checked> Male &nbsp;&nbsp;
			<input type="radio" name="gender" value="female"> Female<br>
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Age in years : </label>
		<div class="w3agile__text col-sm-7 w3agile_banner_btom_login_left1">
			<select id="a_ageFrom" name="a_ageFrom" onchange="advance_from_age()" class="frm-field required">
			<option value="">----------</option>
			<option value="18">18</option><option value="19">19</option>  <option value="20">20</option><option value="21">21</option><option value="22">22</option>   
			<option value="23">23</option><option value="24">24</option><option value="25">25</option>  	
			<option value="26">26</option><option value="27">27</option><option value="28">28</option>  
			<option value="29">29</option><option value="30">30</option><option value="31">31</option>  
			<option value="32">32</option><option value="33">33</option><option value="34">34</option>  
			<option value="35">35</option><option value="36">36</option><option value="37">37</option> 
			<option value="38">38</option><option value="39">39</option><option value="40">40</option>
		</select>
		<span>To </span>
		<select id="a_age_to" name="a_age_to" class="frm-field required">
		</select>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Height : </label>
		<div class="w3agile__text col-sm-7 w3agile_banner_btom_login_left1">
			<select id="a_heightFrom" name="a_height_from" onchange="a_height_From()" class="frm-field required">
			<option value="0">----------</option>
			<option value="4.0">4ft</option><option value="4.1">4ft 1in</option><option value="4.2">4ft 2in</option><option value="4.3">4ft 3in</option><option value="4.4">4ft 4in</option><option value="4.5">4ft 5in</option><option value="4.6">4ft 6in</option><option value="4.7">4ft 7in</option><option value="4.8">4ft 8in</option><option value="4.9">4ft 9in</option><option value="5.0">5ft</option>
		</select>
		<span>To </span>
		<select id="a_heightTo" name="a_height_to" class="frm-field required">
								
		</select>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="marital_status">Marital Status : </label>
		<div class="col-sm-7 form_radios">
			<select name="marital_status" id="marital_status" class="form-control">
	      	<option value="Not married">Not married</option>
	      	<option value="Divorced">Divorced</option>
	      	<option value="Widowed">Widowed</option>
	      	<option value="Separated">Separated</option>
	      </select>
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Religion : </label>
		<div class="col-sm-7 form_radios">
			<div class="select-block1">
				<select name="religion">
					<option value="Muslim">Muslim</option> 
					<option value="Hindu">Hindu</option>
					<option value="Sikh">Sikh</option>
					<option value="Jain">Jain</option>
					<option value="Christian">Christian</option>  
					<option value="Jewish">Jewish</option>
					<option value="Bhuddhist">Bhuddhist</option> 
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Mother Tongue : </label>
		<div class="col-sm-7 form_radios">
			<div class="select-block1">
				<select name="language">
					<option value="Bangla">Bangla</option>
					<option value="English">English</option>
					<option value="Hindi">Hindi</option>
					<option value="Urdu">Urdu</option>
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="form_but1">
		<label class="col-sm-5 control-label1" for="sex">Physical Disability: </label>
		<div class="col-sm-7 form_radios">
			<input type="radio" name="disable" value="No" checked> No &nbsp;&nbsp;
			<input type="radio" name="disable" value="yes"> Yes &nbsp;&nbsp;
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="agileits_advanced_Search">
		<h5>Education and Career</h5>
		<div class="form_but1">
			<label class="col-sm-5 control-label1" for="sex">Highest Education : </label>
			<div class="col-sm-7 form_radios">
				<div class="select-block1">
					<select>
						<option value="Computer Science and Engineering">Computer Science and Engineering</option>
						<option value="Electrical and Electronics Engineering">Electrical and Electronics Engineering</option>
						<option value="Bachelor of Business and Administration">Bachelor of Business and Administration</option>
						<option value="Bachelor of Arts">Bachelor of Arts</option>
						<option value="Management">Any Bachelors/Masters in Management</option>
						<option value="Doctor">Any Bachelors/Masters in Medicine in General / Dental / Surgeon</option>
						<option value="Ph.D">Ph.D.</option>
						<option value="Diploma">Any Diploma</option>
						<option value="">Others</option>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="form_but1">
			<label class="col-sm-5 control-label1" for="sex">Occupation : </label>
			<div class="col-sm-7 form_radios">
				<div class="select-block1">
					<select name="occupation">
						<option value="Teacher">Teacher</option>
						<option value="Engineer">Engineer</option>
						<option value="Doctor">Doctor</option>
						<option value="Business">Business</option>
						<option value="Driver">Driver</option>
						<option value="Student">Student</option>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="form_but1">
			<label class="col-sm-5 control-label1" for="income">Monthly income : </label>
			<div class="col-sm-7 form_radios">
				<div class="select-block1">
					<select name="income" id="income">
						<option value="5000">5000+</option>
						<option value="10000">10,000+</option>
						<option value="15000">15,000+</option>
						<option value="20000">20,000+</option>
						<option value="25000">25,000+</option>
						<option value="30000">30,000+</option>
						<option value="40000">40,000+</option>
						<option value="50000">50,000+</option>
						<option value="60000">60,000+</option>
						<option value="70000">70,000+</option>
						<option value="80000">80,000+</option>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="agileits_advanced_Search">
		<h5>Habbits</h5>
		<div class="form_but1">
			<label class="col-sm-5 control-label1" for="diet">Eating : </label>
			<div class="col-sm-7 form_radios">
				<select name="diet" id="diet" class="form-control">
					<option value="">Select a diet</option>
			      	<option value="Vegitarian">Vegitarian</option>
			      	<option value="Non-vegitarian">Non-vegitarian</option>
			      	<option value="Other">Other</option>
				</select>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<input type="submit" value="Search" />
</form>
<script>
	function advance_from_age(){
    	var xmlHttp=new XMLHttpRequest(); //class
	    xmlHttp.open("GET","ajax_action/fetch_age_to.php?from="+document.getElementById('a_ageFrom').value, false);
	    xmlHttp.send(null);// default function
	    document.getElementById('a_age_to').innerHTML=xmlHttp.responseText;
  }
    function a_height_From(){
    	var xmlHttp=new XMLHttpRequest();
	    xmlHttp.open("GET","ajax_action/fetch_height_to.php?from="+document.getElementById('a_heightFrom').value, false);
	    xmlHttp.send(null);
	    document.getElementById('a_heightTo').innerHTML=xmlHttp.responseText;//for print
  } 
</script>