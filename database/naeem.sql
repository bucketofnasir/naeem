-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2017 at 08:26 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naeem`
--

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE `basic_info` (
  `email` varchar(200) NOT NULL,
  `about` longtext,
  `age` int(11) NOT NULL,
  `marital_status` varchar(50) NOT NULL,
  `diet` varchar(50) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `height` varchar(20) DEFAULT NULL,
  `skin` varchar(10) NOT NULL,
  `hair_color` varchar(20) NOT NULL,
  `eye_color` varchar(20) NOT NULL,
  `freckle` varchar(20) NOT NULL,
  `dimple` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`email`, `about`, `age`, `marital_status`, `diet`, `image`, `height`, `skin`, `hair_color`, `eye_color`, `freckle`, `dimple`) VALUES
('nasirkhancse@gmail.com', 'I am an independent learner. Technology is in my blood. I am a student of a renowned private university in Dhaka under the Department of Computer Science and Engineering. I always like to communicate with the new person and help them in their own way. I always try my level best to create something new. I dislike those who always try to gain a big thing with a shortcut way. I believe that, in IT industry, there is no any shortcut way to become a great programmer. More and more practices make a programmer great and greater.', 22, 'Not Married', 'Non-vegitarian', 'upload/2017-12-01-19-39-5612afebc6a7069fdb6eabaaf405624db8c9a27626', '5.7', 'Medium', 'Black', 'Black', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `email` varchar(200) NOT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `contact_time` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_info`
--

INSERT INTO `contact_info` (`email`, `contact_name`, `contact_number`, `relation`, `contact_time`) VALUES
('nasirkhancse@gmail.com', 'Zannat Nayem', '01789-364588', 'Friend', '10:30PM');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `issue` varchar(50) DEFAULT NULL,
  `comments` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `email`, `name`, `issue`, `comments`) VALUES
(1, 'nasir@gmail.com', 'nsair', 'Category', 'asdfwrwer'),
(2, 'raihanuiu@gmail.com', 'raihan', 'Category', 'sowerasf'),
(3, '', '', 'select', ''),
(4, 'dfg@gmail.com', 'dsdf', 'Category', 'asfewr'),
(5, 'asfsf@gmail.com', 'asdfer', 'Category', 'esfdfg'),
(6, 'asfsf@gmail.com', 'asdfer', 'Category', 'esfdfg'),
(7, 'jhjsdf@fasjf', 'ksajdhf', 'Category', 'sakjfsafh'),
(8, 'hasan_al_jamy@gmail.com', 'hasan al jami', 'Category', 'skjfhwiuersj kjshfisa fksajf'),
(9, 'hasan_al_jamy@gmail.com', 'hasan al jami', 'Category', 'skjfhwiuersj kjshfisa fksajf');

-- --------------------------------------------------------

--
-- Table structure for table `educational_details`
--

CREATE TABLE `educational_details` (
  `email` varchar(200) NOT NULL,
  `undergraduate` varchar(200) DEFAULT NULL,
  `postgraduate` varchar(200) DEFAULT NULL,
  `university` varchar(200) DEFAULT NULL,
  `college` varchar(200) DEFAULT NULL,
  `profession` varchar(100) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `salary` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educational_details`
--

INSERT INTO `educational_details` (`email`, `undergraduate`, `postgraduate`, `university`, `college`, `profession`, `company`, `designation`, `salary`) VALUES
('nasirkhancse@gmail.com', 'Comupter science and engineering', 'Not Completed', 'United international university', 'Dhaka Imperial college', 'Student', 'No company', 'Student', '50000.00');

-- --------------------------------------------------------

--
-- Table structure for table `family_details`
--

CREATE TABLE `family_details` (
  `email` varchar(200) NOT NULL,
  `father_profession` varchar(200) DEFAULT NULL,
  `mother_profession` varchar(200) DEFAULT NULL,
  `brothers` int(11) DEFAULT NULL,
  `sister` int(11) DEFAULT NULL,
  `family_value` varchar(100) DEFAULT NULL,
  `family_type` varchar(50) NOT NULL,
  `family_affluence` varchar(50) NOT NULL,
  `live` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `family_details`
--

INSERT INTO `family_details` (`email`, `father_profession`, `mother_profession`, `brothers`, `sister`, `family_value`, `family_type`, `family_affluence`, `live`) VALUES
('nasirkhancse@gmail.com', 'Job holder', 'Homemaker', 2, 1, 'Moderate', 'Not set', 'Middle class', 'Comilla');

-- --------------------------------------------------------

--
-- Table structure for table `lifestyle`
--

CREATE TABLE `lifestyle` (
  `email` varchar(200) NOT NULL,
  `weight` varchar(200) DEFAULT NULL,
  `habits` varchar(200) DEFAULT NULL,
  `mother_tongue` varchar(200) NOT NULL,
  `language` varchar(200) DEFAULT NULL,
  `blood` varchar(200) DEFAULT NULL,
  `disability` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lifestyle`
--

INSERT INTO `lifestyle` (`email`, `weight`, `habits`, `mother_tongue`, `language`, `blood`, `disability`) VALUES
('nasirkhancse@gmail.com', '68 KG', 'Listening music, programming', 'Bangla', 'English, Hindi, Urdu', 'O+', 'Nope');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `email` varchar(200) NOT NULL,
  `min_age` varchar(200) DEFAULT NULL,
  `max_age` varchar(200) NOT NULL,
  `min_height` varchar(200) DEFAULT NULL,
  `max_height` varchar(200) NOT NULL,
  `partner_marital_status` varchar(200) DEFAULT NULL,
  `partner_religion` varchar(200) DEFAULT NULL,
  `partner_profession` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`email`, `min_age`, `max_age`, `min_height`, `max_height`, `partner_marital_status`, `partner_religion`, `partner_profession`) VALUES
('nasirkhancse@gmail.com', '18', '22', '5.4', '5.6', 'Not Married', 'Islam', 'Any');

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE `privacy` (
  `id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `details` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`id`, `title`, `details`) VALUES
(1, 'What Information Do We Collect?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.'),
(2, 'Description Of Service And Content Policy', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.\r\nLorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(3, 'Do We Use Cookies?', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.'),
(4, 'Conduct', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.\r\nNeque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.\r\nUt enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.'),
(5, 'Third Party Links', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.'),
(6, 'Changes To Our Privacy Policy', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.'),
(7, 'No Spam Policy', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC.'),
(8, 'Limitations Of Liability', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.'),
(9, 'Terms And Conditions', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC.'),
(10, 'Ability To Accept Terms Of Service', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.'),
(11, ' Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.'),
(12, 'How Do We Protect Your Information?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage.'),
(13, 'Contact Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\nMy company name,\r\nLorem ipsum dolor,\r\nJasmin Dr 40 Fe 72.\r\nTel:1115550001\r\nFax:190-4509-494\r\nE-Mail: mail@example.com');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `email` varchar(200) DEFAULT NULL,
  `session_key` varchar(100) DEFAULT NULL,
  `session_start` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `creator` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `dateofbirth` varchar(20) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `visibility` varchar(20) NOT NULL DEFAULT 'Visible'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `creator`, `name`, `email`, `password`, `phone`, `dateofbirth`, `gender`, `religion`, `visibility`) VALUES
(36, 'myself', 'Nasir Khan', 'nasirkhancse@gmail.com', '7b8b965ad4bca0e41ab51de7b31363a1', '01763-433486', '08/03/1995', 'Male', 'Muslim', 'Visible');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educational_details`
--
ALTER TABLE `educational_details`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `family_details`
--
ALTER TABLE `family_details`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `lifestyle`
--
ALTER TABLE `lifestyle`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD CONSTRAINT `basic_info_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD CONSTRAINT `contact_info_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `educational_details`
--
ALTER TABLE `educational_details`
  ADD CONSTRAINT `educational_details_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `family_details`
--
ALTER TABLE `family_details`
  ADD CONSTRAINT `family_details_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `lifestyle`
--
ALTER TABLE `lifestyle`
  ADD CONSTRAINT `lifestyle_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Constraints for table `partner`
--
ALTER TABLE `partner`
  ADD CONSTRAINT `partner_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
