<?php
require 'header.php'; 
if (!isset($_SESSION['email'])) { ?>
	<script>window.location='index.php';</script>
<?php }
$email=$_SESSION['email'];
?>
<style>
	div.page-wrapper{
	background: #f1f1f2!important;
}
.alert{
	font-size: 1.85em;
}

</style>

<div class="page-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
				  <div class="panel-body">
				    <div>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home" id="profile">Profile</a></li>
    <li><a data-toggle="tab" href="#menu1" id="password">Password</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Upload profile picture</h3>
      <div class="imgupinfo">

      	<?php if (isset($_SESSION['upload'])) {
      		echo $_SESSION['upload'];
      		unset($_SESSION['upload']);
      	} ?>
      </div>
      <form action="upload.php" method="POST" enctype="multipart/form-data">
      	<div class="form-group">
      		<?php $sql="SELECT * FROM basic_info WHERE email='$email'";
      		if ($rst=mysqli_query($con, $sql)) {
      			while ($r=mysqli_fetch_assoc($rst)) { ?>
      				<img src="<?php echo($r['image']); ?>" alt="" style="height: 200px;width: 200px;">
      				<input type="file" name="image" value="<?php echo $r['image']; ?>" class="form-control" required>
      			<?php }
      		 } ?>
      	</div>
      <button class="btn btn-primary">upload</button>
      </form>
      <form action="ajax_action/update_profile.php" id="profile" method="POST">
        <div class="panel-body">
		  	<div class="pr_msg"></div>
		  	<?php $sql="SELECT * FROM users WHERE email='$email'";
		  	if ($result=mysqli_query($con,$sql)) {
		  		while ($row=mysqli_fetch_assoc($result)) { ?>
		  		
		  		<div class="form-group creator_group">
		  			<label class="control-label" for="creator">Created for*</label>
		  			<select name="creator" id="creator" class="form-control">
		  				<option value="<?php echo($row['creator']); ?>"><?php echo($row['creator']); ?></option>
		  				<option value="">Select Creator</option>
						<option value="myself">Myself</option>   
						<option value="parent">Son</option>   
						<option value="parent">Daughter</option>   
						<option value="brother">Brother</option>   
						<option value="brother">Sister</option>  
						<option value="relative">Relative</option>
						<option value="friend">Friend</option>
					</select>
		  		</div>
		  		<div class="form-group name_group">
		  			<label class="control-label" for="name">Name*</label>
		  			<input type="text" name="name" id="name" class="form-control" value="<?php echo($row['name']); ?>">
		  		</div>


		  		<div class="form-group email_group">
		  			<label class="control-label" for="email">Email*</label>
		  			<input type="email" name="email" id="email" class="form-control" value="<?php echo($row['email']); ?>" disable>
		  		</div>

		  		<div class="form-group birthdate_group">
		  			<label class="control-label" for="birthdate">Date of birth</label>
		  			<input type="text" name="birthdate" id="birthdate" class="form-control" value="<?php echo($row['dateofbirth']); ?>">
		  		</div>


		  		<div class="form-group phone_group">
		  			<label class="control-label" for="phone">Phone</label>
		  			<input type="text" name="phone" id="phone" class="form-control" value="<?php echo($row['phone']); ?>">
		  		</div>


		  		<div class="form-group">
		  			<button class="btn btn-primary" onclick="saveProfile()">Save Changes</button>
		  		</div>
		  	
		  	<?php	}
		  	 } ?>
		  </div>
		</form>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Change password</h3>
      <div class="panel panel-default">
		  <div class="panel-body">
		  	<div class="msg"></div>
		  	<form action="ajax_action/change_pass.php" id="change_pass" method="POST">
		  		<div class="form-group c_pass_group">
		  			<label class="control-label" for="c_pass">Current password</label>
		  			<input type="password" name="c_pass" id="c_pass" class="form-control">
		  		</div>


		  		<div class="form-group n_pass_group">
		  			<label class="control-label" for="n_pass">New password</label>
		  			<input type="password" name="n_pass" id="n_pass" class="form-control">
		  		</div>


		  		<div class="form-group r_pass_group">
		  			<label class="control-label" for="r_pass">re-enter password</label>
		  			<input type="password" name="r_pass" id="r_pass" class="form-control">
		  		</div>

		  		<div class="form-group">
		  			<button class="btn btn-primary">Change password</button>
		  		</div>
		  	</form>
		  </div>
		</div>
    </div>
  </div>

</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>
<script>
	

	$('form#change_pass').submit(function(event) {
		$('.c_pass_text').remove();
		$('.n_pass_text').remove();
		$('.r_pass_text').remove();
	  	var c_pass = $('#c_pass').val();
	  	var n_pass=$('#n_pass').val();
	  	var r_pass=$('#r_pass').val();
	  	if (c_pass=="") {
	  		$('.c_pass_group').addClass('has-error');
	  		$('#c_pass').after('<div class="text-danger c_pass_text">Password field is required</div>');
	  	}else{
	  		$('.c_pass_group').removeClass('has-error');
	  		$('.c_pass_group').addClass('has-success');
	  		$('.c_pass_text').remove();

	  	}

	  	if (n_pass=="") {
	  		$('.n_pass_group').addClass('has-error');
	  		$('#n_pass').after('<div class="text-danger n_pass_text">*Field is required</div>');
	  	}else{
	  		$('.n_pass_group').removeClass('has-error');
	  		$('.n_pass_group').addClass('has-success');
	  		$('.n_pass_text').remove();
	  	}

	  	if (r_pass=="") {
	  		$('.r_pass_group').addClass('has-error');
	  		$('#r_pass').after('<div class="text-danger r_pass_text">*Field is required</div>');
	  	}else{
	  		$('.r_pass_group').removeClass('has-error');
	  		$('.r_pass_group').addClass('has-success');
	  		$('.r_pass_text').remove();
	  	}

	  	if (c_pass && n_pass && r_pass) {
	  		
	  		event.preventDefault(); // Prevent the form from submitting via the browser
		    var form = $(this);
		    $.ajax({
		      type: form.attr('method'),
		      url: form.attr('action'),
		      data: form.serialize()
		    }).done(function(data) {
		    	$('div.msg').html('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>'+data+'</h4></div>');
		    	$.notify(data,'success');

		    }).fail(function(data) {
		      	$.notify(data,"error");
		    });
	  	 }else{
	  	 	$.notify("Required field needs to fill",'warn');
	  	 	
	  	 	return false;
	  	 }
	  });

	$('form#profile').submit(function(event) {
	  	var name = $('#name').val();
	  	var email=$('#email').val();
	  	var creator=$('#creator').val();
	  	if (name=="") {
	  		$('.name_group').addClass('has-error');
	  	}else{
	  		$('.name_group').removeClass('has-error');
	  		$('.name_group').addClass('has-success');
	  	}

	  	if (email=="") {
	  		$('.email_group').addClass('has-error');
	  	}else{
	  		$('.email_group').removeClass('has-error');
	  		$('.email_group').addClass('has-success');
	  	}

	  	if (creator=="") {
	  		$('.creator_group').addClass('has-error');
	  	}else{
	  		$('.creator_group').removeClass('has-error');
	  		$('.creator_group').addClass('has-success');
	  	}

	  	if (name && email && creator) {
	  		
	  		event.preventDefault(); // Prevent the form from submitting via the browser
		    var form = $(this);
		    $.ajax({
		      type: form.attr('method'),
		      url: form.attr('action'),
		      data: form.serialize()
		    }).done(function(data) {
		    	$('div.pr_msg').html('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>'+data+'</h4></div>');
		    	$.notify(data,'success');

		    }).fail(function(data) {
		      	$.notify(data,"error");
		    });
	  	 }else{
	  	 	$.notify("Required field needs to fill",'warn');
	  	 	$('div.pr_msg').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> Fields are required</div>');
	  	 	return false;
	  	 }
	  });

	//Password change ended here

    $('#file-fr').fileinput({
        theme: 'fa',
        language: 'fr',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $('#file-es').fileinput({
        theme: 'fa',
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $("#file-0").fileinput({
        theme: 'fa',
        'allowedFileExtensions': ['jpg', 'png', 'gif']
    });
    $("#file-1").fileinput({
        theme: 'fa',
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    /*
     $(".file").on('fileselect', function(event, n, l) {
     alert('File Selected. Name: ' + l + ', Num: ' + n);
     });
     */
    $("#file-3").fileinput({
        theme: 'fa',
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-lg",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        overwriteInitial: false,
        initialPreviewAsData: true,
        initialPreview: [
            "http://lorempixel.com/1920/1080/transport/1",
            "http://lorempixel.com/1920/1080/transport/2",
            "http://lorempixel.com/1920/1080/transport/3"
        ],
        initialPreviewConfig: [
            {caption: "transport-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1},
            {caption: "transport-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2},
            {caption: "transport-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3}
        ]
    });
    $("#file-4").fileinput({
        theme: 'fa',
        uploadExtraData: {kvId: '10'}
    });
    $(".btn-warning").on('click', function () {
        var $el = $("#file-4");
        if ($el.attr('disabled')) {
            $el.fileinput('enable');
        } else {
            $el.fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $("#file-4").fileinput('refresh', {previewClass: 'bg-info'});
    });
    /*
     $('#file-4').on('fileselectnone', function() {
     alert('Huh! You selected no files.');
     });
     $('#file-4').on('filebrowse', function() {
     alert('File browse clicked for #file-4');
     });
     */
    $(document).ready(function () {
    	$('ul.nav li a#profile').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
		$('ul.nav li a#password').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
        $("#test-upload").fileinput({
            'theme': 'fa',
            'showPreview': false,
            'allowedFileExtensions': ['jpg', 'png', 'gif'],
            'elErrorContainer': '#errorBlock'
        });
        $("#kv-explorer").fileinput({
            'theme': 'explorer-fa',
            'uploadUrl': '#',
            overwriteInitial: false,
            initialPreviewAsData: true,
            initialPreview: [
                "http://lorempixel.com/1920/1080/nature/1",
                "http://lorempixel.com/1920/1080/nature/2",
                "http://lorempixel.com/1920/1080/nature/3"
            ],
            initialPreviewConfig: [
                {caption: "nature-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1},
                {caption: "nature-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2},
                {caption: "nature-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3}
            ]
        });
        /*
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });
         */

         
         $('input#c_pass').keypress(function(event) {
         	var c_pass= $('input#c_pass').val();
         	if (c_pass) {
         		$('.text-danger.c_pass_text').remove();
         		$('.form-group.c_pass_group').removeClass('has-error');
         		$('.form-group.c_pass_group').addClass('has-success');
         	}
         });
         
         $('input#n_pass').keypress(function(event) {
         	var n_pass= $('input#n_pass').val();
         	if (n_pass) {
         		$('.text-danger.n_pass_text').remove();
         		$('.form-group.n_pass_group').removeClass('has-error');
         		$('.form-group.n_pass_group').addClass('has-success');
         	}
         });
         
         $('input#r_pass').keypress(function(event) {
         	var r_pass= $('input#r_pass').val();
         	if (r_pass) {
         		$('.text-danger.r_pass_text').remove();
         		$('.form-group.r_pass_group').removeClass('has-error');
         		$('.form-group.r_pass_group').addClass('has-success');
         	}
         });

    });
    
</script>