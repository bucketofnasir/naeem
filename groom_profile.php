<?php if (isset($_GET['id'])) {
	$id=$_GET['id'];
	?>
<?php require 'header.php'; ?>
	
	<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="index.php">Home</a> > <a href="matches.php">Matches</a> > <span>Bridegroom Profile Details</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->

	

	<?php $sql="SELECT * FROM users NATURAL JOIN basic_info NATURAL JOIN educational_details NATURAL JOIN family_details NATURAL JOIN lifestyle NATURAL JOIN partner WHERE users.id='$id'";
	if ($result=mysqli_query($con,$sql)) { 
		while ($row=mysqli_fetch_assoc($result)) { 
			$email=$row['email']; ?>

	
	<!-- Bridegroom Profile Details -->
	<div class="w3ls-list">
		<div class="container">
		<h2><?php echo $row['name']; ?> Profile Details</h2>
		<div id="info"></div>
		<div class="col-md-9 profiles-list-agileits">
			<div class="single_w3_profile">
				<div class="agileits_profile_image">
					<img src="<?php echo $row['image']; ?>" alt="profile image" />
				</div>
				<div class="w3layouts_details">
					<h4>Profile ID : <?php echo $id; ?></h4>
					<span>
						<?php $q="SELECT * FROM session WHERE email='$email'";
						$rst=mysqli_query($con, $q);
						if ($rst->num_rows) {
							echo "He is now online";
						 }else{
						 	echo "Last online 3 hours ago";
						 } ?>
					</span>
					<p><?php echo date('Y')-substr($row['dateofbirth'], 6) ?>Years, <?php echo substr($row['height'], 0,1); ?>'<?php echo substr($row['height'], 2,2); ?>" , <?php echo $row['religion']; ?>, <?php echo $row['salary']; ?> BDT, <?php echo $row['profession']; ?></p>
					<?php if (isset($_SESSION['email'])) {
						echo '<a href="#">'.$row['phone'].'</a>';
					}else{
						echo '<a href="#" data-toggle="modal" data-target="#myModal">View Contact</a>';
					} ?>
					<?php if (isset($_SESSION['email'])) { ?>
						<a onclick="sendInterest('<?php echo $row["email"]; ?>');" style="cursor: pointer;">Send interest</a>
					<?php }else{
						echo '<a href="#" data-toggle="modal" data-target="#myModal">Send interest</a>';
					} ?>
					<a href="#" data-toggle="modal" data-target="#myModal">Report Profile</a>
					<a href="#" data-toggle="modal" data-target="#myModal">Block Profile</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="profile_w3layouts_details">
				<div class="agileits_aboutme">
					<h4>About me</h4>
					<h5>Brief about me:</h5>
					<p><?php echo $row['about']; ?></p>
					<h5>Basic info:</h5>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Full name </label>
						<div class="col-sm-9 w3_details">
							: <?php echo $row['name']; ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Gender </label>
						<div class="col-sm-9 w3_details">
							: <?php echo $row['gender']; ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Date of birth </label>
						<div class="col-sm-9 w3_details">
							: <?php if (isset($_SESSION['email'])) {
								echo $row['dateofbirth'];
							}else{
								echo '<span class="text-danger" data-toggle="modal" data-target="#myModal">Login ot see birth date</span>';
							} ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<h5>Family Details:</h5>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Mother : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['mother_profession']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Father : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['father_profession']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Sister's : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['sister']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Brother's : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['brothers']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Family Income : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['salary']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Stay : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['live']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Family Values : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['family_value']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<h5>Education Details:</h5>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">University : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['university']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">UG Degree : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['undergraduate']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">College : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['college']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">PG College : </label>
						<div class="col-sm-9 w3_details">
							Not Specified
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Occupation : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['profession']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Company : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['company']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Designation : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['designation']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<h5>Lifestyle:</h5>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Weight : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['weight']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Height : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['height']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Habits : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['habits']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Languages Known : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['language']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Blood Group : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['blood']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Family Values : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['family_value']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<h5>Desired Partner:</h5>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Age : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['min_age']; ?>-<?php echo $row['max_age']; ?> Years
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Height : </label>
						<div class="col-sm-9 w3_details">
							 <?php echo substr($row['min_height'], 0,1); ?>'<?php echo substr($row['min_height'], 2,2); ?>" to <?php echo substr($row['max_height'], 0,1); ?>'<?php echo substr($row['max_height'], 2,2); ?>"
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Marital Status : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['partner_marital_status']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Religion : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['partner_religion']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="form_but1">
						<label class="col-sm-3 control-label1" for="Relation">Profession : </label>
						<div class="col-sm-9 w3_details">
							<?php echo $row['partner_profession']; ?>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 w3ls-aside">
			<h3>Search by Profile ID:</h3>
			<form action="groom_profile.php" method="get"> 
				<input class="text" type="text" name="id" placeholder="Enter Profile ID" required="">
				<input type="submit" value="Search">
				<div class="clearfix"></div>
			</form>
			<div class="view_profile">
        	<h3>Similar Profiles</h3>
        	<?php
        	$religion=$row['religion'];
        	$gender=$row['gender'];
        	 $sql="SELECT * FROM users NATURAL JOIN basic_info NATURAL JOIN educational_details WHERE id!='$id' AND religion='$religion' AND gender='$gender' LIMIT 0, 10";
        	if ($rst=mysqli_query($con,$sql)) {
        		while ($row=mysqli_fetch_assoc($rst)) { ?>
        			<ul class="profile_item">
		        	  <a href="groom_profile.php?id=<?php echo($row['id']); ?>">
		        	   <li class="profile_item-img">
		        	   	  <img src="<?php echo $row['image']; ?>" class="img-responsive" alt="">
		        	   </li>
		        	   <li class="profile_item-desc">
		        	   	  <h6>ID : <?php echo $row['id']; ?></h6>
		        	   	  <p><?php echo date('Y')-substr($row['dateofbirth'], 6); ?> Yrs, <?php echo substr($row['height'], 0,1); ?>Ft <?php echo substr($row['height'], 2); ?>in <?php echo $row['religion']; ?>,<?php echo $row['salary']; ?>BDT</p>
		        	   </li>
		        	   <div class="clearfix"> </div>
		        	  </a>
		            </ul>	
        	<?php	}
        	 } ?>
        	
       </div>
		</div>
	<div class="clearfix"></div>
	</div>
	
	</div>
	<script src="js/easyResponsiveTabs.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			$('#parentHorizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				closed: 'accordion', // Start closed if in accordion view
				tabidentify: 'hor_1', // The tab groups identifier
				activate: function (event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo');
					var $name = $('span', $info);
		
					$name.text($tab.text());
		
					$info.show();
				}
			});
	 
		});

	</script>
	<!-- //Bridegroom Profile Details -->
	<?php } } 

	?>
	
	
	<!-- browse profiles -->
	<div class="w3layouts-browse text-center">
		<div class="container">
			<h3>Browse Matchmaking Profiles by</h3>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Country</h4>
				<ul>
					<li><a href="nri_list.html">Country 1</a></li>
					<li><a href="nri_list.html">Country 2</a></li>
					<li><a href="nri_list.html">Country 3</a></li>
					<li><a href="nri_list.html">Country 4</a></li>
					<li><a href="nri_list.html">Country 5</a></li>
					<li><a href="nri_list.html">Country 6</a></li>
					<li><a href="nri_list.html">Country 7</a></li>
					<li><a href="nri_list.html">Country 8</a></li>
					<li><a href="nri_list.html">Country 9</a></li>
					<li><a href="nri_list.html">Country 10</a></li>
					<li><a href="nri_list.html">Country 11</a></li>
					<li class="more"><a href="nri_list.html">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Religion</h4>
				<ul>
					<li><a href="r_list.html">Religion 1</a></li>
					<li><a href="r_list.html">Religion 2</a></li>
					<li><a href="r_list.html">Religion 3</a></li>
					<li><a href="r_list.html">Religion 4</a></li>
					<li><a href="r_list.html">Religion 5</a></li>
					<li><a href="r_list.html">Religion 6</a></li>
					<li><a href="r_list.html">Religion 7</a></li>
					<li><a href="r_list.html">Religion 8</a></li>
					<li><a href="r_list.html">Religion 9</a></li>
					<li><a href="r_list.html">Religion 10</a></li>
					<li><a href="r_list.html">Religion 11</a></li>
					<li class="more"><a href="r_list.html">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Community</h4>
				<ul>
					<li><a href="r_list.html">Community 1</a></li>
					<li><a href="r_list.html">Community 2</a></li>
					<li><a href="r_list.html">Community 3</a></li>
					<li><a href="r_list.html">Community 4</a></li>
					<li><a href="r_list.html">Community 5</a></li>
					<li><a href="r_list.html">Community 6</a></li>
					<li><a href="r_list.html">Community 7</a></li>
					<li><a href="r_list.html">Community 8</a></li>
					<li><a href="r_list.html">Community 9</a></li>
					<li class="more"><a href="r_list.html">more..</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //browse profiles -->

	<!-- Get started -->
	<div class="w3layous-story text-center">
		<div class="container">
			<h4>Your story is waiting to happen!  <a href="index.html">Get started</a></h4>
		</div>
	</div>
	<!-- //Get started -->
	
<?php require 'footer.php'; ?>
<script>
		function sendInterest(rec){
			var xmlHttp=new XMLHttpRequest();
		    xmlHttp.open("GET","ajax_action/send_interest.php?receiver="+rec, false);
		    xmlHttp.send(null);
		    document.getElementById('info').innerHTML=xmlHttp.responseText;
	    }

	</script>
<?php }else{
		require '404.php';
	} ?>