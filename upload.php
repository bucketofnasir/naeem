<?php
$con=new mysqli('127.0.0.1','root','','naeem');
session_start();
$email=$_SESSION['email'];
$file=date("Y-m-d-H-i-s").sha1($_FILES['image']['name']);
$destination='upload/'.$file;
$filename=$_FILES['image']['tmp_name'];
if (move_uploaded_file($filename, $destination)) {
  $sql="UPDATE `basic_info` SET `image` = '$destination' WHERE `basic_info`.`email` = '$email'";
  if (mysqli_query($con, $sql)) {
    $_SESSION['upload']= '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Profile picture is uploaded.</div>';
    header('location: settings.php');
  }else{
    $_SESSION['upload']= '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Sorry profile picture is not saved.</div>';
    header('location: settings.php');
  }
}else{
  $_SESSION['upload']= '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry! </strong>Profile picture is not uploaded.</div>';
  header('location: settings.php');
}
?>