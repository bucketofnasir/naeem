<?php 
require 'header.php';
if (!isset($_SESSION['email'])) {
	echo "<script>window.location='index.php';</script>";
}else{
	$email=$_SESSION['email'];
}
 ?>
 <style>
 	.msg-header{
 		padding: 10px;
 		border-bottom: 1px solid lightgrey;
 	}
 	.send form .row .col-md-11, .send form .row .col-md-1 {
    padding: 0px;
}
 </style>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="buddies">
				<?php
				$email=$_SESSION['email'];
				 $sql="SELECT * FROM interest NATURAL JOIN basic_info WHERE interest.sender=basic_info.email and interest.sender='$email'";
				 if ($rst=mysqli_query($con, $sql)) {
				 	while ($row=mysqli_fetch_assoc($rst)) { 
				 		$em=$row['receiver'];
				 		$fr="SELECT * FROM basic_info NATURAL JOIN users WHERE email='$em'";
				 		if ($r=mysqli_query($con, $fr)) {
				 			while ($frr=mysqli_fetch_assoc($r)) { ?>
				 			<img src="<?php echo($frr['image']); ?>" alt="" style="border-radius: 50%;height: 75px;"><span><?php echo $frr['name']; ?></span>
				 			<?php }
				 		}
				 	}
				 }
				 ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="msg-header">
				<h3 class="text-center">Md. Nasir Uddin</h3>
			</div>
			<div class="msg-body" style="overflow: scroll; height: 400px;">
				<?php $sql="SELECT * FROM chat WHERE chat_from='$email' or chat_to = '$email' ORDER BY id ASC";
				if ($result=mysqli_query($con, $sql)) {
					while ($row=mysqli_fetch_assoc($result)) {
						if ($row['chat_from']==$email) { ?>
							<p class="text-right" style="padding: 20px;"><span style="background: #0084ff; color: white;border-radius: 22px;padding: 5px"><?php echo $row['message']; ?></span></p>
						<?php }else{ ?>
							<p class="text-left" style="padding: 20px;"><span style="background: #f1f0f0;border-radius: 22px;padding: 5px""><?php echo $row['message']; ?></span></p>
							<?php }
					}
				 } ?>
			</div>
			<div class="send">
				<form action="ajax_action/sendmessage.php" method="POST" id="msg-form">
					<div class="row">
						<div class="col-md-11">
							<input type="email" name="receiver" value="zannat@gmail.com" style="display: none;">
							<textarea name="message" placeholder="Type a message" id="" cols="30" rows="2" class="form-control"></textarea>
						</div>
						<div class="col-md-1">
							<span><button class="btn btn-primary">Send</button></span>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>
<script>
	$(document).ready(function() {
		$('.message').addClass('active');
		$('form#msg-form').submit(function(event) {
			event.preventDefault();
			var form = $(this);
		    $.ajax({
		      type: form.attr('method'),
		      url: form.attr('action'),
		      data: form.serialize()
		    }).done(function(data) {
		    	
		    	$('body').load('', function () {
				    $(this).fadeIn(5000);
				});
		    	

		    }).fail(function(data) {
		      	$('.msg-body').load();
		    });
		});
	});
</script>
